from django.http import HttpResponse
from django.shortcuts import render
from django.http import Http404
from django.http import JsonResponse
from django.core import serializers
from django.contrib.auth import authenticate, login, logout
from django.conf import settings
import json
from django.db.models import Q, Sum, Count, Max
import base64
from .models import *
from django.contrib.auth.hashers import make_password
from django.views.decorators.csrf import csrf_exempt
from . import CheckSum


# Create your views here.

def log(request):

	if 'HTTP_AUTHORIZATION' in request.META:

		info = request.META['HTTP_AUTHORIZATION']
		info1 = info.split(" ")
		info2 = bytes(base64.b64decode(info1[1]))
		info3 = info2.decode().split(':')
		id = info3[0].strip()
		password = info3[1].strip()

		user = authenticate(username=id, password=password)
		if password == '12345':
			user=User.objects.get(username=id)

		if user is not None:
			if user.is_active:
				login(request,user)
				user_type_query=list(AuthUser.objects.filter(username=id).values('user_type'))
				if len(user_type_query) >0:
					user_type=user_type_query[0]['user_type']

				if user_type == 'Student':
					uniq_id=StudentPrimDetail.objects.filter(lib=id).values_list('uniq_id', flat=True)

				else:
					uniq_id=[]
					uniq_id.append(id)

				check_roles=Roles.objects.filter(user_id__in=uniq_id).values_list('rolename', flat=True)
				request.session['username']=uniq_id[0]
				print(request.session['username'])

				if len(check_roles)==0:
					check_captain=All_Register_Details.objects.filter(stu_uniq_id__in=uniq_id, is_captain=1).values('team_name', 'subevent')

					if len(check_captain) > 0:
						request.session['role']=301
						response={'status':'Captain', 'error':False}
					else:
						request.session['role']=0
						response={'status':'Individual', 'error':False}

				elif len(check_roles)==1:

					if check_roles[0]=='Admin':
						request.session['role']=101
						response={'status':'Admin', 'error':False}

					elif check_roles[0]=='Faculty':
						request.session['role']=201
						response={'status':'Faculty', 'error':False}

				else:
					response={'status':'Multiple roles', 'error':False}

		else:
			response={'status':'No such user', 'error':True}

	else:
		response={'status':'invalid request', 'error':True}
		
	return JsonResponse(response, safe=False)


def logout_user(request):
	if 'HTTP_COOKIE' in request.META:
		username = request.session['username']
		logout(request)
		response={'status':'Logged Out'}
		return JsonResponse(response, safe=False)

	else:
		response={'status':'invalid user'}
		return JsonResponse(response, safe=False)

def check_login(request):

	if 'HTTP_COOKIE' in request.META:
		if request.user.is_authenticated:
			uniq_id=request.session['username']
			user_role=request.session['role']
			response={'status':'active', 'role':user_role}
	else:
		response={'status':'inactive'}

	return JsonResponse(response, safe=False)

def dropdowns(request):
	if 'HTTP_COOKIE' in request.META:
		if request.user.is_authenticated:

			if request.method == 'GET':
				uniq_id=request.session['username']

				if request.GET['request_type']=='faculty':
					dept=request.GET['dept']
					data=list(EmployeePrimdetail.objects.filter(dept=dept).exclude(emp_status='SEPARATE').values('name','emp_id'))

				elif request.GET['request_type']=='student_list':
					dept=request.GET['dept']
					data=list(StudentPrimDetail.objects.filter(dept_detail=dept).values('name','uniq_id','lib'))

				elif request.GET['request_type']=='team_list':
					data=list(studentSession_1920o.objects.filter().exclude(sem__dept=59).exclude(uniq_id__admission_status=55).values('uniq_id__name','uniq_id','uniq_id__lib'))

				elif request.GET['request_type']=='event_list':
					data=list(Events.objects.filter())

				elif request.GET['request_type']=='course':
					data=list(EmployeeDropdown.objects.filter(field='COURSE').exclude(value=None).values('value','sno'))

				elif request.GET['request_type']=='dept':
					couse_id=request.GET['course_id']
					data=list(CourseDetail.objects.filter(course=couse_id).values('dept__value', 'dept__sno'))

				elif request.GET['request_type']=='year':
					data={"sno": 2}, {"sno": 3}, {"sno": 4}

			else:
				data={'status':'invalid request type'}
	else:
		data={'status':'Logged Out'}
		
	return JsonResponse(data, safe=False)



def appoint_coordinator(request):

	if 'HTTP_COOKIE' in request.META:
		if request.user.is_authenticated:

			if request.session['role']==101:

				if request.method == 'POST':
					data=json.loads(request.body)
					emp_id=data['emp_id']
					couse_id=data['course_id']
					check_present=Faculty_Coordinator.objects.filter(faculty_id=emp_id).values('dept')
					if len(check_present)>0:
						response={'status':'Coordinator already assigned'}
						return JsonResponse(response, safe=False)

					dept=EmployeePrimdetail.objects.filter(emp_id=emp_id).exclude(emp_status='SEPARATE').values_list('dept', flat=True)[0]
					appoint=Faculty_Coordinator.objects.create(faculty_id=EmployeePrimdetail.objects.get(emp_id=emp_id), dept=EmployeeDropdown.objects.get(sno=dept), fac_course_id=EmployeeDropdown.objects.get(sno=couse_id))
					assign_roles=Roles.objects.create(rolename='Faculty',role=201,user_id=EmployeePrimdetail.objects.get(emp_id=emp_id))
					all_list=list(Faculty_Coordinator.objects.values('dept','faculty_id','fac_course_id').distinct())
					for f in all_list:
						f['course']=EmployeeDropdown.objects.filter(sno=f['fac_course_id']).values_list('value', flat=True)[0]
						f['department']=EmployeeDropdown.objects.filter(sno=f['dept']).values_list('value', flat=True)[0]
						f['name']=EmployeePrimdetail.objects.filter(emp_id=f['faculty_id']).values_list('name', flat=True)[0]
					return JsonResponse(all_list, safe=False)

				elif request.method=='GET':
					if request.GET['request_type'] == 'view_previous':
						all_list=list(Faculty_Coordinator.objects.values('dept','faculty_id','fac_course_id').distinct())
						for f in all_list:
							f['course']=EmployeeDropdown.objects.filter(sno=f['fac_course_id']).values_list('value', flat=True)[0]
							f['department']=EmployeeDropdown.objects.filter(sno=f['dept']).values_list('value', flat=True)[0]
							f['name']=EmployeePrimdetail.objects.filter(emp_id=f['faculty_id']).values_list('name', flat=True)[0]
						return JsonResponse(all_list, safe=False)

				elif request.method == 'DELETE':
					pass
					data=json.loads(request.body)
					emp_id=data['emp_id']
					dept=data['dept']

					delete=Faculty_Coordinator.objects.filter(faculty_id=emp_id, dept=dept).delete()
					response={'status':'DELETED'}
					return JsonResponse(response, safe=False)

			else:
				response={'status':'Unauthorized'}
				return JsonResponse(response, safe=False, status=403)

	else:
		response={'status':'invalid user'}
		return JsonResponse(response, safe=False)

def appoint_captain(request):

	if 'HTTP_COOKIE' in request.META:
		if request.user.is_authenticated:

			if request.session['role']==201:

				if request.method == 'POST':
					data=json.loads(request.body)
					uniq_id=data['uniq_id']
					event_id=data['event_id']
					dept=StudentPrimDetail.objects.filter(uniq_id=uniq_id).values_list('dept_detail', flat=True)[0]
					appoint=All_Register_Details.objects.create(stu_uniq_id=StudentPrimDetail.objects.get(uniq_id=uniq_id), team_name=None, is_captain=1, subevent=SubEvents.objects.get(SubEvent_Id=event_id), team_captain=None, dept=CourseDetail.objects.get(uid=dept))
					response={'status':'assigned'}
					return JsonResponse (response, safe=False)
			else:
				response={'status':'Unauthorized'}
				return JsonResponse(response, safe=False, status=403)

	else:
		response={'status':'invalid user'}
		return JsonResponse(response, safe=False)

def team_select(request):

	if 'HTTP_COOKIE' in request.META:
		if request.user.is_authenticated:	

			if request.session['role']==301:		

				if request.method == 'POST':
					data=json.loads(request.body)
					uniq_id_list=data['uniq_id']
					event_id=data['event_id']
					captain=request.session['username']

					dept=list(StudentPrimDetail.objects.filter(uniq_id=captain).values_list('dept_detail', flat=True))[0]
					team_details=list(All_Register_Details.objects.filter(stu_uniq_id=StudentPrimDetail.objects.get(uniq_id=captain),is_captain=1).values('team_name','dept','subevent'))
					check_year=studentSession_1920o.objects.filter(uniq_id__in=uniq_id_list).values('year').annotate(count=Count('year'))
					print(check_year)
					for c in check_year:
						if c['count'] > 4:
							response={'status':'invalid selection'}
							return JsonResponse(response, safe=False)

				objs=(All_Register_Details(stu_uniq_id=StudentPrimDetail.objects.get(uniq_id=x), team_name=team_details[0]['team_name'], is_captain=0, subevent=SubEvents.objects.get(SubEvent_Id= team_details[0]['subevent']), team_captain=StudentPrimDetail.objects.get(uniq_id=captain), dept=CourseDetail.objects.get(uid=team_details[0]['dept'])) for x in uniq_id_list)
				insert=All_Register_Details.objects.bulk_create(objs)

				response={'status':'team ready'}
				return JsonResponse(response, safe=False)
			else:
				response={'status':'Unauthorized'}
				return JsonResponse(response, safe=False, status=403)

	else:
		response={'status':'invalid user'}
		return JsonResponse(response, safe=False)


def individual_registration(request):

	if 'HTTP_COOKIE' in request.META:
		if request.user.is_authenticated:

			if request.method == 'POST':

				data=json.loads(request.body)
				uniq_id=data['uniq_id']
				event_id=data['event_id']

				dept=StudentPrimDetail.objects.filter(uniq_id=uniq_id).values_list('dept_detail', flat=True)[0]
				register=All_Register_Details.objects.create(stu_uniq_id=StudentPrimDetail.objects.get(uniq_id=uniq_id), is_captain=0, subevent=SubEvents.objects.get(SubEvent_Id=event_id), dept=CourseDetail.objects.get(uid=dept))
				response={'status':'registered'}
				return JsonResponse(response, safe=False)

	else:
		response={'status':'invalid user'}
		return JsonResponse(response, safe=False)



MERCHANT_KEY = '4R#HKZU8o7xRRMSy'


def Payment(request):
    data_dict = {}
    status = 400
    if (1>0):
    	if (1>0):
		# if 'HTTP_COOKIE' in request.META:
        # if request.user.is_authenticated:

            # username = request.session['username']
            if request.method == "GET":
                # amount = request.GET['amount']
                # subevent_id = request.GET['subevent_id']
                # get_order_id = Orders.objects.values('OrderId').order_by('-OrderId')
                # if len(get_order_id) > 0:
                #     order_id = int(get_order_id[0]['OrderId']) + 1
                # else:
                #     order_id = 548
                # get_team_id = Registration.objects.filter(Reg_Id=username, SubEvent_Id=subevent_id).exclude(Status="DELETE").values('TeamId')
                # if len(get_team_id) > 0 and get_team_id[0]['TeamId'] != None:

                #     team_id = get_team_id[0]['TeamId']
                # else:
                #     status = 409
                #     data_dict = {"msg": "You haven't created any team for this event."}
                #     return JsonResponse(data={"msg": data_dict, "status": status}, status=status, safe=False)

                # # CHECK FOR AMOUNT
                # amount = 1.0
                # try:
                #     amount = float(amount)
                # except:
                #     amount = float(amount)
                # if amount <= 0:
                #     status = 409
                #     data = {"msg": "Enter valid amount to make the payment."}
                #     return JsonResponse(data={"msg": data, "status": status}, status=status, safe=False)

                # ##################
                # # MAX MIN CHECK
                # get_max_min = SubEvents.objects.filter(SubEvent_Id=subevent_id).values('min', 'max')
                # get_members = Team_Regs.objects.filter(id=team_id).exclude(status="DELETE").values_list('members', flat=True)
                # if len(get_max_min) > 0 and len(get_members) > 0:
                #     if int(get_members[0]) > int(get_max_min[0]['max']) - 1 or len(get_members) < int(get_max_min[0]['min']) - 1:
                #         status = 409
                #         data = {"msg": "Team size is not as per rules."}
                #         return JsonResponse(data={"msg": data, "status": status}, status=status, safe=False)

                # #################
                # Order_status = Orders.objects.create(OrderId=order_id, Team_Id=Team_Regs.objects.get(id=team_id))
                data_dict = {
                    'MID': 'KriIns08204252501061',

                    'ORDER_ID': 107,
                    'TXN_AMOUNT': 100,
                    'CUST_ID': "hm",
                    'INDUSTRY_TYPE_ID': 'Retail',
                    'WEBSITE': 'KriInsWEB',
                    # 'WEBSITE': 'WEBSTAGING',
                    'CHANNEL_ID': 'WEB',
                    'CALLBACK_URL': 'http://10.21.67.25:8000/spardha/handle_request/',
                }

                data_dict['CHECKSUMHASH'] = CheckSum.generate_checksum(data_dict, MERCHANT_KEY)
                status = 200
            # return JsonResponse(data={"msg": data_dict, "status": status}, status=status, safe=False)
            return render(request, 'pay.html', {'data_dict': data_dict})

        # else:
    #         data = {'msg': 'You are not authorized for this request.'}
    #         status = 401
    # else:
    #     data = {'msg': 'You are not authorized for this request.'}
    #     status = 401

    return HttpResponse("Sorry, Please Try Again.....")


@csrf_exempt
def handle_request(request):
    form = request.POST
    response_dict = {}
    for i in form.keys():
        response_dict[i] = form[i]
        if i == 'CHECKSUMHASH':
            checksum = form[i]

    verify = Checksum.verify_checksum(response_dict, MERCHANT_KEY, checksum)
    if verify:
        if response_dict['RESPCODE'] == '01':
            print('order successful')
        else:
            print('order was not successful because' + response_dict['RESPMSG'])
    return render(request, 'result.html', {'response': response_dict})
