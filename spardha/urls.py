from django.conf.urls import url
from .views import *

urlpatterns=[
	
	url(r'^login/$' , log),
	url(r'^logout/$' , logout_user),
	url(r'^appoint_coordinator/$', appoint_coordinator),
	url(r'^appoint_captain/$', appoint_captain),
	url(r'^dropdowns/$', dropdowns),
	url(r'^team_select/$', team_select),
	url(r'^check_login/$', check_login),
	url(r'^individual_registration/$', individual_registration),
	url(r'^payment/$', Payment),
	url(r'^handle_request/$', handle_request),
]