from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class EmployeeDropdown(models.Model):
    sno = models.AutoField(db_column='Sno', primary_key=True)  # Field name made lowercase.
    pid = models.IntegerField(db_column='Pid', blank=True, null=True)  # Field name made lowercase.
    field = models.CharField(db_column='Field', max_length=500, blank=True, null=True)  # Field name made lowercase.
    value = models.CharField(db_column='Value', max_length=500, blank=True, null=True)  # Field name made lowercase.
    is_edit = models.IntegerField(db_column='Is_Edit',default='1')  # Field name made lowercase.
    is_delete = models.IntegerField(db_column='Is_Delete',default='1')  # Field name made lowercase.
    status = models.CharField(db_column='status', max_length=50, blank=True, null=True,default='INSERT')  # Field name made lowercase.
    

    class Meta:
        db_table = 'employee_dropdown'

# class StudentDropdown(models.Model):
#     sno = models.AutoField(db_column='Sno', primary_key=True)  # Field name made lowercase.
#     pid = models.IntegerField(db_column='Pid', blank=True, null=True)  # Field name made lowercase.
#     field = models.CharField(db_column='Field', max_length=500, blank=True, null=True)  # Field name made lowercase.
#     value = models.CharField(db_column='Value', max_length=500, blank=True, null=True)  # Field name made lowercase.
#     is_edit = models.IntegerField(db_column='Is_Edit',default='1')  # Field name made lowercase.
#     is_delete = models.IntegerField(db_column='Is_Delete',default='1')  # Field name made lowercase.
#     status = models.CharField(max_length=10, blank=True, null=True)

#     class Meta:
#         managed = True
#         db_table = 'Student_Dropdown'


class CourseDetail(models.Model):
    uid = models.AutoField(db_column='id', primary_key=True)  # Field name made lowercase.
    # course_duration = models.IntegerField(db_column='Course_duration')  # Field name made lowercase.
    course = models.ForeignKey(EmployeeDropdown,related_name='StudentDropdown_Course_id', db_column='Course_id', blank=True, null=True,on_delete=models.SET_NULL)  # Field name made lowercase.
    # course_type = models.ForeignKey(StudentDropdown,related_name='StudentDropdown_Course_type', db_column='Course_type', blank=True,null=True,on_delete=models.SET_NULL)  # Field name made lowercase.
    dept = models.ForeignKey(EmployeeDropdown,related_name='EmployeeDropdown_Dept_id', db_column='Dept_id', blank=True, null=True,on_delete=models.SET_NULL)  # Field name made lowercase.
    # capacity = models.IntegerField(db_column='capacity',default=0)  # Field name made lowercase.
    # lateral_capacity = models.IntegerField(db_column='lateral_capacity',default=0)  # Field name made lowercase.
    
    class Meta:
        managed = True
        db_table = 'Student_course_detail'

class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()
    user_type = models.CharField(max_length=255)

    class Meta:
        managed=True
        db_table = 'authenticate_user'


class EmployeePrimdetail(models.Model):
    title = models.ForeignKey(EmployeeDropdown, related_name='title',db_column='Title', blank=True, null=True, limit_choices_to={'field':'TITLE'},on_delete=models.SET_NULL)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dept = models.ForeignKey(EmployeeDropdown,db_column='Dept', related_name='department', blank=True, null=True, limit_choices_to={'field':'DEPARTMENT'},on_delete=models.SET_NULL)  # Field name made lowercase.
    # doj = models.DateField(db_column='DOJ', blank=True, null=True)  # Field name made lowercase.
    current_pos = models.ForeignKey(EmployeeDropdown, related_name='Current_Position',db_column='Current_Pos', blank=True, null=True,on_delete=models.SET_NULL)  # Field name made lowercase.
    # emp_type = models.ForeignKey(EmployeeDropdown, related_name='Emp_Type', blank=True,db_column='Emp_Type', null=True, limit_choices_to={'Field':'TYPE OF EMPLOYEMENT'},on_delete=models.SET_NULL)  # Field name made lowercase.
    emp_category = models.ForeignKey(EmployeeDropdown, related_name='Emp_Category', blank=True, null=True,db_column='emp_category', limit_choices_to={'Field':'CATEGORY OF EMPLOYEE'},on_delete=models.SET_NULL)  # Field name made lowercase.
    desg = models.ForeignKey(EmployeeDropdown, related_name='designation',db_column='Desg', blank=True, null=True, limit_choices_to={'Field':'DESIGNATION'},on_delete=models.SET_NULL)  # Field name made lowercase.
    # cadre = models.ForeignKey(EmployeeDropdown, related_name='cadre',db_column='Cadre', blank=True, null=True, limit_choices_to={'Field':'CADRE'},on_delete=models.SET_NULL)  # Field name made lowercase.
    # ladder = models.ForeignKey(EmployeeDropdown, related_name='ladder',db_column='Ladder', blank=True, null=True, limit_choices_to={'Field':'LADDER'},on_delete=models.SET_NULL)  # Field name made lowercase.
    # shift = models.ForeignKey(EmployeeDropdown, related_name='shift',db_column='Shift', blank=True, null=True, limit_choices_to={'Field':'SHIFT'},on_delete=models.SET_NULL)  # Field name made lowercase.
    mob = models.CharField(db_column='Mob', max_length=12, blank=True, null=True)  # Field name made lowercase.
    mob1 = models.CharField(db_column='Mob1', max_length=12, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='Email', max_length=100, blank=True, null=True)  # Field name made lowercase.
    emp_id = models.OneToOneField(User,to_field='username',db_column='Emp_Id', primary_key=True,unique=True,on_delete=models.CASCADE)  # Field name made lowercase.
    lib_card_no = models.CharField(db_column='Lib_Card_No', max_length=15, blank=True, null=True)  # Field name made lowercase.
    # organization = models.ForeignKey(EmployeeDropdown, related_name='organization', blank=True, null=True, limit_choices_to={'Field':'SHIFT'},on_delete=models.SET_NULL,db_column='Organization')  # Field name made lowercase.
    emp_status = models.CharField(db_column='Emp_Status', max_length=255, blank=True, null=True, default='ACTIVE')  # Field name made lowercase.
    # join_pos = models.ForeignKey('EmployeeDropdown',related_name='Join_Position',db_column='Join_Pos', blank=True, null=True,on_delete=models.SET_NULL)  # Field name made lowercase.

    class Meta:
        db_table = 'employee_primdetail'

class StudentPrimDetail(models.Model):
    name = models.CharField(db_column='Name', max_length=500)  # Field name made lowercase.
    # batch_from = models.IntegerField(db_column='Batch_from')  # Field name made lowercase.
    # batch_to = models.IntegerField(db_column='Batch_to')  # Field name made lowercase.
    # exam_roll_no = models.CharField(db_column='Exam_Roll_No', max_length=100)  # Field name made lowercase.
    # general_rank = models.IntegerField(db_column='General_Rank', blank=True, null=True)  # Field name made lowercase.
    # category_rank = models.IntegerField(db_column='Category_Rank', blank=True, null=True)  # Field name made lowercase.
    # gen_rank = models.IntegerField(db_column='Gen_Rank', blank=True, null=True)  # Field name made lowercase.
    uni_roll_no = models.CharField(db_column='Uni_Roll_No', max_length=15, blank=True, null=True)  # Field name made lowercase.
    join_year = models.IntegerField(db_column='Join_Year')  # Field name made lowercase.
    email_id = models.CharField(db_column='Email_id', max_length=500, blank=True, null=True)  # Field name made lowercase.
    # date_of_add = models.DateField(db_column='Date_Of_Add')  # Field name made lowercase.
    uniq_id = models.AutoField(db_column='Uniq_Id', primary_key=True)  # Field name made lowercase.
    # old_uniq_id = models.CharField(db_column='old_uniq_id', max_length=500, blank=True, null=True,default=None)  # Field name made lowercase.
    # form_fill_status = models.CharField(db_column='Form_Fill_Status', max_length=2,default='N')  # Field name made lowercase.
    # fee_waiver = models.CharField(db_column='Fee_Waiver', max_length=41, default='N')  # Field name made lowercase.
    # remark = models.CharField(max_length=500, blank=True, null=True)
    # remark_detail = models.CharField(max_length=500,db_column='remark_detail', blank=True, null=True, default=None)
    # admission_category = models.ForeignKey(StudentDropdown,related_name='StudentPrimDetail_StudentDropdown_Admission_category', db_column='Admission_category', blank=True, null=True,on_delete=models.SET_NULL)  # Field name made lowercase.
    # admission_through = models.ForeignKey(StudentDropdown,related_name='StudentPrimDetail_StudentDropdown_Admission_through', db_column='Admission_through', blank=True, null=True,on_delete=models.SET_NULL)  # Field name made lowercase.
    admission_type = models.ForeignKey(EmployeeDropdown,related_name='StudentPrimDetail_StudentDropdown_Admission_type', db_column='Admission_type', blank=True, null=True,on_delete=models.SET_NULL)  # Field name made lowercase.
    dept_detail = models.ForeignKey(CourseDetail,related_name='StudentPrimDetail_CourseDetail_Dept_detail', db_column='Dept_detail', blank=True, null=True,on_delete=models.SET_NULL)  # Field name made lowercase.
    # exam_type = models.ForeignKey(StudentDropdown,related_name='StudentPrimDetail_StudentDropdown_Exam_type', db_column='Exam_type', blank=True, null=True,on_delete=models.SET_NULL)  # Field name made lowercase.
    lib = models.ForeignKey(AuthUser,to_field='username',related_name='StudentPrimDetail_AuthUser_Lib_id', db_column='Lib_id', blank=True, null=True,on_delete=models.SET_NULL)  # Field name made lowercase.
    # stu_type = models.ForeignKey(StudentDropdown,related_name='StudentPrimDetail_StudentDropdown_Stu_Type', db_column='Stu_Type', blank=True, null=True,on_delete=models.SET_NULL)  # Field name made lowercase.
    admission_status = models.ForeignKey(EmployeeDropdown,related_name='StudentPrimDetail_StudentDropdown_admission_status', db_column='admission_status', blank=True, null=True,default=52,on_delete=models.SET_NULL)
    # caste = models.ForeignKey(EmployeeDropdown,related_name='StudentPerDetail_StudentDropdown_Caste', db_column='Caste', blank=True, null=True,on_delete=models.SET_NULL)  # Field name made lowercase.
    gender = models.ForeignKey(EmployeeDropdown,related_name='StudentPerDetail_StudentDropdown_Gender', db_column='Gender', blank=True, null=True,on_delete=models.SET_NULL)  # Field name made lowercase.
    # old_uniq_id=models.CharField(max_length=50)
    time_stamp=models.DateTimeField(db_column='time_stamp',auto_now=True)
    
    class Meta:
        managed = True
        db_table = 'Student_primdetail'


# class Semtiming(models.Model):
#     uid = models.IntegerField(db_column='Uid', primary_key=True)  # Field name made lowercase.
#     session = models.CharField(max_length=15, blank=True, null=True)
#     sem_type = models.CharField(max_length=20, blank=True, null=True)
#     sem_end = models.DateField(blank=True, null=True)
#     sem_start = models.DateField(blank=True, null=True)
#     session_name = models.CharField(max_length=25)

#     class Meta:
#         managed = True
#         db_table = 'sem_timing'


class StudentSemester(models.Model):
    sem_id = models.AutoField(db_column='Sem_Id', primary_key=True)  # Field name made lowercase.
    sem = models.IntegerField(db_column='Sem')  # Field name made lowercase.
    dept = models.ForeignKey(CourseDetail,related_name='CourseDetail_Dept', db_column='Dept',null=True,on_delete=models.SET_NULL)  # Field name made lowercase.

    class Meta:
        managed = True
        db_table = 'student_semester'


# class Sections(models.Model):
#     section_id = models.AutoField(db_column='Section_id', primary_key=True)  # Field name made lowercase.
#     section = models.CharField(db_column='Section', max_length=45)  # Field name made lowercase.
#     # groups = models.IntegerField(db_column='Groups', blank=True,null=True)  # Field name made lowercase.
#     sem_id = models.ForeignKey(StudentSemester,related_name='Sections_StudentSemester_Sections_sem_id',db_column='Sem_id',null=True,on_delete=models.SET_NULL)
#     dept=models.ForeignKey(CourseDetail,related_name='Sections_CourseDetail_Sections_dept',db_column='Dept_detail',null=True,on_delete=models.SET_NULL)
#     status = models.CharField(max_length=10, blank=True, null=True)

#     class Meta:
#         managed = True
#         db_table = 'sections'



class studentSession_1920o(models.Model):
    uniq_id = models.OneToOneField(StudentPrimDetail,related_name='studentSession_1920o_StudentPrimDetail_Uniq_Id', db_column='Uniq_Id', primary_key=True,on_delete=models.CASCADE)  # Field name made lowercase.
    mob = models.BigIntegerField(db_column='Mob', blank=True, null=True)  # Field name made lowercase.
    # fee_status = models.CharField(db_column='Fee_Status', max_length=20, blank=True, null=True)  # Field name made lowercase.
    # session=models.ForeignKey(Semtiming,related_name='S1920Session',db_column='session',null=True,on_delete=models.SET_NULL)
    year = models.IntegerField(db_column='Year', blank=True, null=True)  # Field name made lowercase.
    # class_roll_no = models.IntegerField(db_column='Class_Roll_No', blank=True, null=True)  # Field name made lowercase.
    # registration_status = models.IntegerField(db_column='Registration_Status', blank=True, null=True,default=1)  # Field name made lowercase.
    # acc_reg = models.IntegerField(db_column='Acc_Reg', blank=True, null=True)  # Field name made lowercase.
    # section = models.ForeignKey(Sections,related_name='studentSession_1920o_Sections_Section_id', db_column='Section_id', blank=True, null=True,on_delete=models.SET_NULL)  # Field name made lowercase.
    sem = models.ForeignKey(StudentSemester,related_name='studentSession_1920o_Semtiming_Sem_id', db_column='Sem_id', blank=True, null=True,on_delete=models.SET_NULL)  # Field name made lowercase.
    # att_start_date = models.DateField(default=None,null=True)
    ############# by default it will be equal to semester commencement date as defined by DEAN ACADEMICS for that semester ###########
    # reg_form_status = models.CharField(max_length=10,default='LOCK')
    ############### LOCK OR UNLOCK #######################################
    # reg_date_time = models.DateTimeField(default=None,null=True)
    
    class Meta:
        managed = True
        db_table = 'studentSession_1920o'

class Roles(models.Model):
    rolename = models.CharField(db_column='Rolename', max_length=100)
    role = models.IntegerField(db_column='Roles',blank=True, null=True)
    user_id=models.ForeignKey(EmployeePrimdetail, related_name='Emp_id_list', blank=True, null=True, on_delete=models.SET_NULL)
    class Meta:
        managed = True
        db_table = 'roles'


class Events(models.Model):
    Eid = models.AutoField(db_column='Eid', primary_key=True)  # Field name made lowercase.
    Ename = models.CharField(db_column='Ename', max_length=500, blank=True, null=True)
    Etype = models.CharField(db_column='Etype', max_length=50, blank=True, null=True)
    Rules = models.CharField(db_column='Rules', max_length=5000, blank=True, null=True)

    class Meta:
        db_table = 'Events'


class SubEvents(models.Model):
    SubEvent_Id = models.AutoField(db_column='SubEvent_Id', primary_key=True)
    Eid = models.ForeignKey(Events, related_name='Event_Id', db_column='Eid', blank=True, null=True, on_delete=models.SET_NULL)  # Field name made lowercase.
    SubEvent = models.CharField(db_column='SubEvent', max_length=500, blank=True, null=True)
    min = models.IntegerField(db_column='min',  blank=True, null=True)
    max = models.IntegerField(db_column='max',  blank=True, null=True)
    Amount = models.IntegerField(db_column='Amount_kiet',  blank=True, null=True)
    judging = models.CharField(db_column='judging', max_length=50, blank=True, null=True)
    schedule = models.CharField(db_column='schedule', max_length=50, blank=True, null=True)
    contact = models.CharField(db_column='contact', max_length=11, blank=True, null=True)
    Image = models.TextField(db_column='Image', blank=True, null=True)

    class Meta:
        db_table = 'SubEvents'


	
class Faculty_Coordinator(models.Model):
    faculty_id=models.ForeignKey(EmployeePrimdetail, related_name="Faculty", blank=True, null=True, on_delete=models.SET_NULL)
    dept = models.ForeignKey(EmployeeDropdown, related_name="Department", blank=True, null=True, on_delete=models.SET_NULL)
    fac_course_id = models.ForeignKey(EmployeeDropdown, related_name="fac_course_id", blank=True, null=True, on_delete=models.SET_NULL)
    class Meta:
        db_table='Faculty_Coordinator'

class All_Register_Details(models.Model):
    stu_uniq_id=models.ForeignKey(StudentPrimDetail, related_name="Uniq_Id", blank=True, null=True, on_delete=models.SET_NULL)
    team_name=models.CharField(max_length=200, blank=True, null=True)
    is_captain=models.IntegerField()
    subevent=models.ForeignKey(SubEvents, related_name="Subevent_Id_Detail", blank=True, null=True, on_delete=models.SET_NULL)
    team_captain=models.ForeignKey(StudentPrimDetail, related_name="Team_Captain", blank=True, null=True, on_delete=models.SET_NULL)
    dept=models.ForeignKey(CourseDetail, related_name="Dept_Details_list", blank=True, null=True, on_delete=models.SET_NULL)
    status=models.CharField(db_column="Status", max_length=100, default='UNPAID')		

    class Meta:
        db_table='All_Register_Details'

class Orders(models.Model):
    Id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    OrderId = models.CharField(db_column='OrderId', max_length=50,  blank=True, null=True)
    # Team_Id = models.ForeignKey(Team_Regs, db_column='Team_Id', related_name="Order_Team_Id", blank=True, null=True, on_delete=models.SET_NULL)  # Field name made lowercase.
    TransactionId = models.CharField(db_column='TransactionId', max_length=500, blank=True, null=True)
    status = models.CharField(db_column='status', max_length=50, blank=True, null=True, default='PENDING')
    timestamp = models.DateTimeField(auto_now=True)  # Field name made lowercase.

    class Meta:
        db_table = 'Orders'
